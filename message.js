// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAJjm0h4uTGNK9Ak8Mt9QyUBsRELFdqY8U",
    authDomain: "claire-e47c1.firebaseapp.com",
    databaseURL: "https://claire-e47c1.firebaseio.com",
    projectId: "claire-e47c1",
    storageBucket: "claire-e47c1.appspot.com",
    messagingSenderId: "1041520386288",
    appId: "1:1041520386288:web:b2656862b81ab822d8a235"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//Firebase Databas Reference and the child
const dbRef = firebase.database().ref(); //Faire le lien avec notre base de données
const userRef = dbRef.child("users"); //Tu te connectes directement sur les users

    readUserData();

function readUserData(){
    
    let userListUi = document.getElementById("user-list");
    
    usersRef.on("value", snap => {
            
        userListUi.innerHTML = "";
            
        snap.forEach(childSnap =>{
            let key = childSnap.key;
            let value = childSnap.val();
            let $li = document.createElement("li");
    
            //edit icon
            let editIconUi = document.createElement("span");
            editIconUi.className = "edit-user";
            editIconUi.innerHTML = " ✎ ";
            editIconUi.setAttribute("userid", key);
            //editIconUi.addEventListener("click", editButtonClicked);
                
            $li.innerHTML = value.name;
            $li.append(editIconUi);
                
            $li.setAttribute("user-key", key);
            $li.addEventListener("click", userClicked);
            userListUi.append($li);
                
        });
    })
}

// --------------------------
// DISPLAY USER INFO
// --------------------------    

function userClicked(e){
    let userID = e.target.getAttribute("user-key");
    
    let userRef = dbRef.child("users/" + userID);
    let userDetailUi = document.getElementById("user-detail");
        
    userRef.on("value", snap =>{
            
        userDetailUi.innerHTML = "";
            
        snap.forEach(childSnap =>{
            let $p = document.createElement("p");
            $p.innerHTML = childSnap.key + " - " + childSnap.val();
            userDetailUi.append($p);
        })
    })
}

let addUserBtnUi = document.getElementById("add-user-btn");
addUserBtnUi.addEventListener("click", addUserBtnCliked);

function addUserBtnCliked(){
    let usersRef = dbRef.child("users/");
    let addUserInputsUi = document.getElementsByClassName("user-input");
    //cte objet va stocker les info du nouvel utilisateur
    let newUser = {};
    //on fait une boucle pour récupérer les valeurs de chaque input après le formulaire
    for(let i=0 ; i<addUserInputsUi.length ; i++){
        //On récupère les key et valeurs
        let key = addUserInputsUi[i].getAttribute("data-key");
        let value = addUserInputsUi[i].value;
        //pour chaque clé (age, name et mail, on les associe à notre nouvel utilisateur)
        newUser[key] = value;
    }
    //on ajoute notre nouvel utilisateur dans la BD
    usersRef.push(newUser);
    console.log("User Saved");
    console.log( `${newUser.name} il a ${newUser.age} ans.`);
}

function readUserData(){
    
    let userListUi = document.getElementById("user-list");

    usersRef.on("value", snap => {
        
        userListUi.innerHTML = "";
        
        snap.forEach(childSnap =>{
            let key = childSnap.key;
            let value = childSnap.val();
            let $li = document.createElement("li");

            //edit icon
            let editIconUi = document.createElement("span");
            editIconUi.className = "edit-user";
            editIconUi.innerHTML = " Modif";
            editIconUi.setAttribute("userid", key);
            //editIconUi.addEventListener("click", editButtonClicked);
            
            $li.innerHTML = value.name;
            $li.append(editIconUi);
            
            $li.setAttribute("user-key", key);
            $li.addEventListener("click", userClicked);
            userListUi.append($li);
            
        });
    })
}