// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD0wG49MmsyVYFl-J7Z65fnPOH02kh07JE",
    authDomain: "super-base09.firebaseapp.com",
    databaseURL: "https://super-base09.firebaseio.com",
    projectId: "super-base09",
    storageBucket: "super-base09.appspot.com",
    messagingSenderId: "192376462115",
    appId: "1:192376462115:web:d8b71641758e0200ff4a96",
    measurementId: "G-C4DJWZ5JDL"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//firebase.analytics();
//const database = firebase.database();


const dbRef = firebase.database().ref();

const usersRef = dbRef.child("users");

readUserData();


function readUserData(){
    
    let userListUi = document.getElementById("user-list");

    usersRef.on("value", snap => {
        
        userListUi.innerHTML = "";
        
        snap.forEach(childSnap =>{
            let key = childSnap.key;
            let value = childSnap.val();
            let $li = document.createElement("li");

            //edit icon
            let editIconUi = document.createElement("span");
            editIconUi.className = "edit-user";
            editIconUi.innerHTML = " ✎ ";
            editIconUi.setAttribute("userid", key);
            editIconUi.addEventListener("click", editButtonClicked);

            //delete icon
            let deleteIconUi = document.createElement("span");
            deleteIconUi.className = "delete-user";
            deleteIconUi.innerHTML = " ⓧ";
            deleteIconUi.setAttribute("userid", key);
            deleteIconUi.addEventListener("click", deleteButtonClicked);
            
            $li.innerHTML = value.name;
            $li.append(editIconUi);
            $li.append(deleteIconUi);
            
            $li.setAttribute("user-key", key);
            $li.addEventListener("click", userClicked);
            userListUi.append($li);
            
        });
    })
}

function deleteButtonClicked(e){
    let deleteID = e.target.getAttribute("userid");
    let userRef = dbRef.child("users/" + deleteID);
    userRef.remove();
}

function editButtonClicked(e){
    document.getElementById("edit-user-module").style.display = "block";
    document.querySelector(".edit-userid").value = e.target.getAttribute("userid");
    let userRef = dbRef.child("users/" + e.target.getAttribute("userid"));
    let editUserInputsUi = document.querySelectorAll(".edit-user-input");

    userRef.on("value", snap=>{
        for (let i = 0 ; i< editUserInputsUi.length ; i++){
            let key = editUserInputsUi[i].getAttribute("data-key");
            editUserInputsUi[i].value = snap.val()[key];
        }
        let saveBtn = document.querySelector("#edit-user-btn");
        saveBtn.addEventListener("click", saveUserBtnClicked);
    })
}

function saveUserBtnClicked(e){
    let userID = document.querySelector(".edit-userid").value;
    let userRef = dbRef.child("users/" + userID);

    let editedUserObject = {};

    let editUserInputsUi = document.querySelectorAll(".edit-user-input");
    
    for (let i=0 ; i<editUserInputsUi.length ; i++){
        let key = editUserInputsUi[i].getAttribute("data-key");
        let value = editUserInputsUi[i].value;
        editedUserObject[key] = value;
    };

    userRef.update(editedUserObject);
    console.log(editedUserObject)
    console.log("Modification effectuée");
    document.getElementById("edit-user-module").style.display = "none";
}

function userClicked(e){
    let userID = e.target.getAttribute("user-key");

    let userRef = dbRef.child("users/" + userID);
    let userDetailUi = document.getElementById("user-detail");
    
    userRef.on("value", snap =>{
        
        userDetailUi.innerHTML = "";
        
        snap.forEach(childSnap =>{
            let $p = document.createElement("p");
            $p.innerHTML = childSnap.key + " - " + childSnap.val();
            userDetailUi.append($p);
        })
    })
}

let addUserBtnUi = document.getElementById("add-user-btn");
addUserBtnUi.addEventListener("click", addUserBtnCliked);

function addUserBtnCliked(){
    let usersRef = dbRef.child("users/");
    let addUserInputsUi = document.getElementsByClassName("user-input");
    //cte objet va stocker les info du nouvel utilisateur
    let newUser = {};
    //on fait une boucle pour récupérer les valeurs de chaque input après le formulaire
    for(let i=0 ; i<addUserInputsUi.length ; i++){
        //On récupère les key et valeurs
        let key = addUserInputsUi[i].getAttribute("data-key");
        let value = addUserInputsUi[i].value;
        //pour chaque clé (age, name et mail, on les associe à notre nouvel utilisateur)
        newUser[key] = value;
    }
    //on ajoute notre nouvel utilisateur dans la BD
    usersRef.push(newUser);
    console.log("User Saved");
    console.log( `${newUser.name} il a ${newUser.age} ans.`);
}